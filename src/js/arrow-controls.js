export class ArrowControls {
  constructor () {
    this.arrowKeyCodes = {
      'up': 38,
      'down': 40,
      'left': 37,
      'right': 39
    }
  }

  registerListener (key, keyFunc) {
    window.addEventListener('keyup', (e) => {
      if (e.keyCode === this.arrowKeyCodes[key]) {
        keyFunc()
      }
    })
  }
}
