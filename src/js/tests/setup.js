const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')

chai.use(sinonChai)

// Set up test globals for convenience
global.sinon = sinon
global.expect = chai.expect
