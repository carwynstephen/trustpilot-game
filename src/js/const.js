export const apiUrl = 'https://ponychallenge.trustpilot.com'

/**
 * Color palette
*/
export const colors = {
  'blue': '#3a3d72',
  'gold': '#b88c03',
  'green': '#47A025'
}

/**
 * Maze dimensions
*/
export const mazeDimensions = {
  blockSize: 50,
  mazeWidth: 15,
  mazeHeight: 15,
  wallSize: 5
}

/**
 * Character dimensions
*/

export const characterDimensions = {
  width: 30,
  height: 30
}
