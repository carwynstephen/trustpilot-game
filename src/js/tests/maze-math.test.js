import { blockToXY } from 'maze-math'

describe('Maze math functions', () => {
  describe('block to XY choordinates', () => {
    it ('uses the maze width/height to convert a block number to xy choordinates', () => {
      const mazeWidth = 5
      const mazeHeight = 5

      expect(
        blockToXY.bind(undefined, 26, mazeWidth, mazeHeight)
      ).to.throw('The block number provided can\'t be inside this maze')
      expect(blockToXY(1, mazeWidth, mazeHeight)).to.deep.equal({x: 1, y: 0})
      expect(blockToXY(5, mazeWidth, mazeHeight)).to.deep.equal({x: 0, y: 1})
    })
  })
})
