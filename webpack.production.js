const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const common = require(path.join(__dirname, 'webpack.common.js'))

module.exports = merge(common, {
  plugins: [
    new UglifyJsPlugin({
      test: /\.js?$/
    })
  ]
})
