import { Game } from 'game'

import 'game.scss'

const newGameButton = document.getElementById('newGameButton')

newGameButton.addEventListener('click', () => {
  window.localStorage.removeItem('mazeId')
  location.reload()
})

const game = new Game('game')

game.run()
