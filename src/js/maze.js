import Konva from 'konva'

import { colors } from 'const'
import { blockToXY } from 'maze-math'
import { getMazeId, getMaze } from 'api-client'

export class Maze {
  constructor (renderer, dataRepo, mazeDimensions) {
    const { mazeWidth, mazeHeight } = mazeDimensions

    Object.assign(this, { renderer, dataRepo, mazeWidth, mazeHeight })

    this.canMove = this.canMove.bind(this)

    this.mazeId = window.localStorage.mazeId
    this.renderer.drawMaze()
  }

  // Get maze data via the API client
  async initializeFromApi (ponyName) {
    const mazeId = await this.initializeMazeId(ponyName)

    const response = await getMaze(mazeId)

    const { data: wallData } = response.data

    this.setWallData(wallData)
    this.setEndPoint(response.data['end-point'][0])

    return response
  }

  async initializeMazeId (ponyName) {
    if (!window.localStorage.mazeId) {
      const response = await getMazeId(ponyName, this.mazeWidth, this.mazeHeight)
      this.mazeId = response.data.maze_id
      window.localStorage.mazeId = this.mazeId
    }

    return window.localStorage.mazeId
  }

  setWallData (wallData) {
    this.dataRepo.setWallData(wallData)
    this.renderer.drawWalls(wallData)
  }

  setEndPoint (exitBlock) {
    this.dataRepo.setEndPoint(exitBlock)
    this.renderer.drawEndPoint(exitBlock)
  }

  canMove (fromBlock, direction) {
    return this.dataRepo.canMove(fromBlock, direction)
  }
}

export class MazeData {
  constructor ({ mazeWidth, mazeHeight }) {
    this.wallData = []
    this.endPoint = 0

    Object.assign(this, { mazeWidth, mazeHeight })
  }

  setWallData (wallData) {
    this.wallData = wallData
  }

  setEndPoint (exitBlock) {
    this.endPoint = exitBlock
  }

  canMove (fromBlock, direction) {
    let toBlock, toWalls

    const fromWalls = this.wallData[fromBlock]

    switch (direction) {
    case 'north':
      if (fromBlock < this.mazeWidth) {
        return false
      } else {
        if (fromWalls.includes('north')) {
          return false
        }
      }
      break
    case 'east':
      if (fromBlock % this.mazeWidth === this.mazeWidth - 1) {
        return false
      } else {
        toBlock = fromBlock + 1
        toWalls = this.wallData[toBlock]
        if (toWalls.includes('west')) {
          return false
        }
      }
      break
    case 'west':
      if (fromBlock % this.mazeWidth === 0) {
        return false
      } else {
        toBlock = fromBlock - 1
        toWalls = this.wallData[toBlock]
        if (fromWalls.includes('west')) {
          return false
        }
      }
      break
    case 'south':
      if (fromBlock > (this.mazeWidth - 1) * this.mazeHeight) {
        return false
      } else {
        toBlock = fromBlock + this.mazeWidth
        toWalls = this.wallData[toBlock]
        if (toWalls.includes('north')) {
          return false
        }
      }
      break
    }

    return true
  }
}

export class MazeRenderer {
  constructor (stage, { mazeWidth, mazeHeight, blockSize, wallSize }) {
    Object.assign(this, { mazeWidth, mazeHeight, blockSize, wallSize })

    this.stage = stage
    this.background = new Konva.Layer()
    this.mazeWalls = new Konva.Layer()
    this.endPoint = new Konva.Layer()
  }

  drawMaze () {
    let mazeSquare = new Konva.Rect({
      x: 0,
      y: 0,
      width: this.blockSize * this.mazeWidth,
      height: this.blockSize * this.mazeHeight,
      fill: colors['blue']
    })

    this.background.add(mazeSquare)
    this.stage.add(this.background)
  }

  drawWalls (wallData) {
    wallData.forEach((blockWalls, index) => {
      const { x: xPos, y: yPos } = blockToXY(index, this.mazeWidth, this.mazeHeight)

      this.addWallsToBlock([xPos, yPos], blockWalls)

      if (xPos + 1 === this.mazeWidth) {
        this.addWallsToBlock([xPos, yPos], ['east'])
      }

      if (yPos === this.mazeHeight - 1) {
        this.addWallsToBlock([xPos, yPos], ['south'])
      }
    })

    this.stage.add(this.mazeWalls)
  }

  addWallsToBlock (blockGridChoords, walls) {
    const [ blockX, blockY ] = blockGridChoords

    walls.forEach((wall) => {
      let x, y, isVertical
      switch (wall) {
      case 'north':
        isVertical = false
        x = blockX * this.blockSize
        y = blockY * this.blockSize
        break
      case 'east':
        isVertical = true
        x = (blockX + 1) * this.blockSize - this.wallSize
        y = blockY * this.blockSize
        break
      case 'south':
        isVertical = false
        x = blockX * this.blockSize
        y = (blockY + 1) * this.blockSize - this.wallSize
        break
      case 'west':
        isVertical = true
        x = blockX * this.blockSize
        y = blockY * this.blockSize
        break
      }

      this.drawWall([x, y], { isVertical })
    })
  }

  drawWall (choords, { isVertical }, color) {
    let [ x, y ] = choords

    let wall = new Konva.Rect({
      x,
      y,
      width: isVertical ? this.wallSize : this.blockSize,
      height: isVertical ? this.blockSize + this.wallSize : this.wallSize,
      fill: color || colors['gold']
    })

    this.mazeWalls.add(wall)
  }

  drawBlock (choords, color) {
    let [ x, y ] = choords

    let wall = new Konva.Rect({
      x: x + this.wallSize,
      y: y + this.wallSize,
      width: this.blockSize - this.wallSize,
      height: this.blockSize - this.wallSize,
      fill: color || colors['gold']
    })

    this.endPoint.add(wall)
  }

  drawEndPoint (block) {
    const {x, y} = blockToXY(block, this.mazeWidth, this.mazeHeight)

    this.drawBlock([x * this.blockSize, y * this.blockSize], colors['green'])

    this.stage.add(this.endPoint)
  }
}
