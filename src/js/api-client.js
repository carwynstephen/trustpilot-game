import { apiUrl } from 'const'
import axios from 'axios'

export const getMazeId = (name, mazeWidth, mazeHeight) => {
  return axios.post(`${apiUrl}/pony-challenge/maze`, {
    'maze-width': mazeWidth,
    'maze-height': mazeHeight,
    'maze-player-name': name,
    'difficulty': 0
  })
}

export const getMaze = (mazeId) => {
  return axios.get(`${apiUrl}/pony-challenge/maze/${mazeId}`)
}

export const updateMaze = (mazeId, direction) => {
  return axios.post(`${apiUrl}/pony-challenge/maze/${mazeId}`, {
    direction
  })
}
