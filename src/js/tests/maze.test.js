import { Maze, MazeData, MazeRenderer } from 'maze'
import { mazeDimensions } from 'const'

describe('Maze', () => {
  // Create spies for MazeData/MazeRenderer
  const mazeRendererStub = sinon.createStubInstance(MazeRenderer)
  const mazeDataStub = sinon.createStubInstance(MazeData)

  it("calls the renderer's draw method on initialization", () => {
    const mockMaze = new Maze(mazeRendererStub, mazeDataStub, mazeDimensions)

    expect(mazeRendererStub.drawMaze.calledOnce).to.be.true
  })

  it('sets wall data and calls relevant method on the renderer', () => {
    const mockMaze = new Maze(mazeRendererStub, mazeDataStub, mazeDimensions)

    const wallData = []

    mockMaze.setWallData(wallData)

    expect(mazeDataStub.setWallData.calledOnce).to.be.true
    expect(mazeRendererStub.drawWalls.calledOnce).to.be.true
  })

  it('sets the end point data and renders it', () => {
    const mockMaze = new Maze(mazeRendererStub, mazeDataStub, mazeDimensions)

    const endPoint = 20

    mockMaze.setEndPoint(endPoint)

    expect(mazeDataStub.setEndPoint.calledOnce).to.be.true
    expect(mazeRendererStub.drawEndPoint.calledOnce).to.be.true
  })
})

describe('MazeData', () => {
  it ('can determine whether or not a block can be moved into', () => {
    // Create a mock wallData object with four blocks in it.
    const wallData = [['north', 'west'], ['north'], ['north', 'west'], []]

    const testMazeDimensions = {
      mazeWidth: 2,
      mazeHeight: 2,
    }

    // Create a mock 2x2 maze to consume our wallData
    const mazeData = new MazeData(testMazeDimensions)
    mazeData.setWallData(wallData)

    let currentBlock = 0

    expect(mazeData.canMove(currentBlock, 'north')).to.be.false
    expect(mazeData.canMove(currentBlock, 'east')).to.be.true
    expect(mazeData.canMove(currentBlock, 'west')).to.be.false
    expect(mazeData.canMove(currentBlock, 'south')).to.be.false

    currentBlock = 3

    expect(mazeData.canMove(currentBlock, 'north')).to.be.true
    expect(mazeData.canMove(currentBlock, 'east')).to.be.false
    expect(mazeData.canMove(currentBlock, 'west')).to.be.true
    expect(mazeData.canMove(currentBlock, 'south')).to.be.false
  })
})
