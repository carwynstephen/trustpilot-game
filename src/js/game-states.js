import Konva from 'konva'

import { apiUrl } from 'const'

export class GameStateRenderer {
  constructor (stage, { mazeWidth, mazeHeight, blockSize }) {
    Object.assign(this, { stage, mazeWidth, mazeHeight, blockSize })

    this.state = new Konva.Layer()
  }

  renderState (gameState) {
    const imageObj = new window.Image()

    let stateImage

    imageObj.onload = () => {
      stateImage = new Konva.Image({
        x: 0,
        y: 0,
        image: imageObj,
        width: this.mazeWidth * this.blockSize,
        height: this.mazeHeight * this.blockSize
      })

      this.stage.clear()
      this.state.add(stateImage)
      this.stage.add(this.state)
    }

    imageObj.src = `${apiUrl}/${gameState['hidden-url']}`
  }
}
