import Konva from 'konva'

import { updateMaze } from 'api-client'
import { blockToXY } from 'maze-math'

export class Character {
  constructor (
    imgPath,
    renderer,
    charBlock,
    moveValidatorFunc) {
    Object.assign(this, { renderer, charBlock, moveValidatorFunc })

    this.renderer.draw(imgPath, charBlock)
  }

  updateBlock (charBlock) {
    this.charBlock = charBlock
  }

  async move (mazeId, direction) {
    if (this.moveValidatorFunc(this.charBlock, direction)) {
      await updateMaze(window.localStorage.mazeId, direction)
    }
  }

  updatePosition (charBlock) {
    this.renderer.setPosition(charBlock)
  }
}

export class CharacterRenderer {
  constructor (stage, { width, height }, { blockSize, mazeWidth, mazeHeight }) {
    Object.assign(this, { stage, width, height, mazeWidth, mazeHeight, blockSize })

    this.setPosition = this.setPosition.bind(this)

    this.layer = new Konva.Layer()
  }

  draw (imgPath, charBlock) {
    const imageObj = new window.Image()

    const { x: xPos, y: yPos } = blockToXY(charBlock, this.mazeWidth, this.mazeHeight)

    imageObj.onload = () => {
      this.charImage = new Konva.Image({
        x: xPos * this.blockSize + 12,
        y: yPos * this.blockSize + 12,
        image: imageObj,
        width: this.width,
        height: this.height
      })

      this.layer.add(this.charImage)

      this.stage.add(this.layer)
    }

    imageObj.src = imgPath
  }

  move (direction) {
    let x, y

    switch (direction) {
    case 'north':
      x = 0
      y = 1 * this.blocksize
      break
    case 'south':
      x = 0
      y = 1 * this.blockSize
      break
    case 'west':
      x = -1 * this.blockSize
      y = 0
      break
    case 'east':
      x = 1 * this.blockSize
      y = 0
      break
    }

    this.charImage.move({x, y})
  }

  setPosition (charBlock) {
    const { x, y } = blockToXY(charBlock, this.mazeWidth, this.mazeHeight)

    this.charImage.position({ x: x * this.blockSize + 12, y: y * this.blockSize + 12 })
    this.stage.draw()
  }
}

export class Characters {
  constructor (stage, characterDimensions, mazeDimensions) {
    this.characters = {}

    Object.assign(this, {stage, characterDimensions, mazeDimensions})
  }

  add (name, imgPath, charBlock, moveValidatorFunc) {
    this.characters[name] = new Character(
      imgPath,
      new CharacterRenderer(this.stage, this.characterDimensions, this.mazeDimensions),
      charBlock,
      moveValidatorFunc)

    return this.characters[name]
  }

  getCharacter (name) {
    return this.characters[name]
  }

  updatePositions (positions) {
    Object.keys(positions).forEach((charName) => {
      let char = this.getCharacter(charName)

      if (char) {
        char.updatePosition(positions[charName])
        char.updateBlock(positions[charName])
      }
    })
  }
}
