import Konva from 'konva'

import { Maze, MazeRenderer, MazeData } from 'maze'
import { ArrowControls } from 'arrow-controls'
import { GameStateRenderer } from 'game-states'
import { Characters } from 'character'
import { mazeDimensions, characterDimensions } from 'const'
import { getMaze } from 'api-client'

// Make sure to import character assets so they end up in the dist folder.
import 'rarity.png'
import 'domo-kun.jpg'

export class Game {
  constructor (canvasElementId) {
    const { blockSize, mazeWidth, mazeHeight } = mazeDimensions

    Object.assign(this, { mazeWidth, mazeHeight })

    this.stage = new Konva.Stage({
      container: canvasElementId,
      width: blockSize * mazeWidth,
      height: blockSize * mazeHeight
    })

    this.statesRenderer = new GameStateRenderer(this.stage, mazeDimensions)

    this.controls = new ArrowControls()

    this.mazeRenderer = new MazeRenderer(this.stage, mazeDimensions)
    this.mazeDataRepo = new MazeData(mazeDimensions)
    this.maze = new Maze(this.mazeRenderer, this.mazeDataRepo, mazeDimensions)

    this.characters = new Characters(this.stage, characterDimensions, mazeDimensions)
  }

  async updateGameState () {
    const response = await getMaze(this.maze.mazeId)

    const { pony, domokun } = response.data
    const gameState = response.data['game-state']

    if (gameState.state !== 'active') {
      this.statesRenderer.renderState(gameState)
    } else {
      this.characters.updatePositions({
        'pony': pony[0],
        'domokun': domokun[0]
      })
    }
  }

  async moveCharThenUpdate (character, direction) {
    await character.move(this.maze.mazeId, direction)

    this.updateGameState()
  }

  registerCharacterControls (character) {
    this.controls.registerListener('up', () => this.moveCharThenUpdate(character, 'north'))
    this.controls.registerListener('down', () => this.moveCharThenUpdate(character, 'south'))
    this.controls.registerListener('left', () => this.moveCharThenUpdate(character, 'west'))
    this.controls.registerListener('right', () => this.moveCharThenUpdate(character, 'east'))
  }

  async run () {
    // Get maze ID, then the maze data, set all relevant data accordingly
    const response = await this.maze.initializeFromApi('rarity', this.mazeWidth, this.mazeHeight)

    const { pony, domokun } = response.data
    const gameState = response.data['game-state']

    if (gameState.state !== 'active') {
      this.statesRenderer.renderState(gameState)
    }

    // Once we have character data, create the characters
    this.characters.add('pony', 'img/rarity.png', pony[0], this.maze.canMove)
    this.characters.add('domokun', 'img/domo-kun.jpg', domokun[0], this.maze.canMove)

    this.registerCharacterControls(this.characters.getCharacter('pony'))
  }
}
