# Trustpilot Save the Pony Challenge
 
This is my attempt at solving the challenge :) to install, run `npm install` followed by `npm run build` and then open dist/index.html.

For the challenge I decided to use vanilla JS with a library to handle drawing to Canvas/handle AJAX requests. I used webpack to bundle all the assets that can be found under `src`, and wrote tests for the game which can be found under `src/js/tests`

## How to play
Control the pony with the arrow keys, get the pony to the green block!