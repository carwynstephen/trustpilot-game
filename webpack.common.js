const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry: {
    main: ["babel-polyfill", path.join(__dirname, 'src/js/main.js')]
  },

  output: {
    filename: 'js/[name].js',
    path: path.join(__dirname, 'docs'),
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: true
          }
        },
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{
            loader: 'css-loader',
            options: {
              import: 1
            }
          }, {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              includePaths: [
                './resources/assets/styles'
              ]
            }
          }]
        })
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'img/[name].[ext]'
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('./css/game.css')
  ],

  resolve: {
    modules: ['node_modules', path.join(__dirname, 'src/js'), path.join(__dirname, 'src/assets'), path.join(__dirname, 'src/scss')],
    extensions: ['.js', '.jsx', '.json']
  }
}
