// Get the X and Y position in the maze from a block number
export const blockToXY = (blockNo, mazeWidth, mazeHeight) => {
  if (blockNo > mazeWidth * mazeHeight) {
    throw Error('The block number provided can\'t be inside this maze')
  }

  const x = blockNo % mazeWidth
  const y = Math.floor(blockNo / mazeHeight)

  return { x, y }
}
